# Scroll Job

## _Job search for _**a**_-literates_

By:
* Agneska <agneska@agneska.fr>
* Jordan Charlier <jordan.a.charlier@gmail.com>
* Marie-Pierre Duclos <mduclos@student.42.fr>
* Sarah Huxhol <sarah.huxhol@covea.fr>
* Emmanuel Raviart <emmanuel@raviart.com>

Copyright (C) 2019 Agneska, Jordan Charlier, Marie-Pierre Duclos, Sarah Huxhol & Emmanuel Raviart

https://framagit.org/maneska/scrolljob.git

> Scroll Job is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Scroll Job is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
