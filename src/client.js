import * as sapper from "@sapper/app"

sapper.start({
  target: document.querySelector("#sapper"), // eslint-disable-line no-undef
})
